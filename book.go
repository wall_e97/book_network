package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-protos-go/peer"
)

type BookTraceChaincode struct {
}

type book struct {
	ObjectType         string `json:"docType"`
	PedigreeRegistrationNumber      string `json:"pedigreeRegistrationNumber"`
	Breed       string `json:"breed"`
	BirthDate       int    `json:"birthDate"`
	Owner              string `json:"owner"`
}

func main() {
	err := shim.Start(new(BookTraceChaincode))
	if err != nil {
		fmt.Printf("Error starting Books Trace chaincode: %s", err)
	}
}

func (t *BookTraceChaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *BookTraceChaincode) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	if function == "initBook" { //create a new book
		return t.initBook(stub, args)
	} else if function == "transferBook" { //change owner of a specific book
		return t.transferBook(stub, args)
	} else if function == "readBook" { //read a book
		return t.readBook(stub, args)
	} else if function == "deleteBook" { //delete a book
		return t.deleteBook(stub, args)
	}

	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error("Received unknown function invocation")
}

func (t *BookTraceChaincode) initBook(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var err error

	// data model
	//   0       		1      		2     		3
	// "book1000001", "harrypotter", "1502688979", "hunvg"

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	// ==== Input sanitation ====
	fmt.Println("- start init book")
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string")
	}
	if len(args[3]) <= 0 {
		return shim.Error("5th argument must be a non-empty string")
	}


	pedigreeRegistrationNumber := args[0]
	breed := strings.ToLower(args[1])
	birthDate, err := strconv.Atoi(args[2])
	if err != nil {
		return shim.Error("2nd argument must be a numeric string")
	}
	owner := strings.ToLower(args[3])

	// ==== Check if book already exists ====
	bookAsBytes, err := stub.GetState(pedigreeRegistrationNumber)
	if err != nil {
		return shim.Error("Failed to get book: " + err.Error())
	} else if bookAsBytes != nil {
		return shim.Error("This book already exists: " + pedigreeRegistrationNumber)
	}

	// ==== Create book object and marshal to JSON ====
	objectType := "book"
	book := &book{objectType, pedigreeRegistrationNumber, breed, birthDate, owner}
	bookJSONasBytes, err := json.Marshal(book)
	if err != nil {
		return shim.Error(err.Error())
	}

	// === Save book to state ===
	err = stub.PutState(pedigreeRegistrationNumber, bookJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	// ==== Book saved and indexed. Return success ====
	fmt.Println("- end init book")
	return shim.Success(nil)
}

// ===============================================
// createIndex - create search index for ledger
// ===============================================
func (t *BookTraceChaincode) createIndex(stub shim.ChaincodeStubInterface, indexName string, attributes []string) error {
	fmt.Println("- start create index")
	var err error

	indexKey, err := stub.CreateCompositeKey(indexName, attributes)
	if err != nil {
		return err
	}

	value := []byte{0x00}
	stub.PutState(indexKey, value)

	fmt.Println("- end create index")
	return nil
}


func (t *BookTraceChaincode) deleteIndex(stub shim.ChaincodeStubInterface, indexName string, attributes []string) error {
	fmt.Println("- start delete index")
	var err error

	indexKey, err := stub.CreateCompositeKey(indexName, attributes)
	if err != nil {
		return err
	}
	//  Delete index by key
	stub.DelState(indexKey)

	fmt.Println("- end delete index")
	return nil
}

func (t *BookTraceChaincode) readBook(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var pedigreeRegistrationNumber, jsonResp string
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting pedigree number of the book to query")
	}

	pedigreeRegistrationNumber = args[0]
	valAsbytes, err := stub.GetState(pedigreeRegistrationNumber) //get the book from chaincode state
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + pedigreeRegistrationNumber + "\"}"
		return shim.Error(jsonResp)
	} else if valAsbytes == nil {
		jsonResp = "{\"Error\":\"Book does not exist: " + pedigreeRegistrationNumber + "\"}"
		return shim.Error(jsonResp)
	}

	return shim.Success(valAsbytes)
}


func (t *BookTraceChaincode) deleteBook(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	var jsonResp string
	var bookJSON book
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	pedigreeRegistrationNumber := args[0]

	valAsbytes, err := stub.GetState(pedigreeRegistrationNumber) //get the book from chaincode state
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + pedigreeRegistrationNumber + "\"}"
		return shim.Error(jsonResp)
	} else if valAsbytes == nil {
		jsonResp = "{\"Error\":\"Book does not exist: " + pedigreeRegistrationNumber + "\"}"
		return shim.Error(jsonResp)
	}

	err = json.Unmarshal([]byte(valAsbytes), &bookJSON)
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to decode JSON of: " + pedigreeRegistrationNumber + "\"}"
		return shim.Error(jsonResp)
	}

	err = stub.DelState(pedigreeRegistrationNumber) //remove the book from chaincode state
	if err != nil {
		return shim.Error("Failed to delete state:" + err.Error())
	}

	return shim.Success(nil)
}

func (t *BookTraceChaincode) transferBook(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	//   0       1       3
	// "name", "from", "to"
	if len(args) < 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}

	pedigreeRegistrationNumber := args[0]
	currentOnwer := strings.ToLower(args[1])
	newOwner := strings.ToLower(args[2])
	fmt.Println("- start transferBook ", pedigreeRegistrationNumber, currentOnwer, newOwner)

	message, err := t.transferBookHelper(stub, pedigreeRegistrationNumber, currentOnwer, newOwner)
	if err != nil {
		return shim.Error(message + err.Error())
	} else if message != "" {
		return shim.Error(message)
	}

	fmt.Println("- end transferBook (success)")
	return shim.Success(nil)
}

func (t *BookTraceChaincode) transferBookHelper(stub shim.ChaincodeStubInterface, pedigreeRegistrationNumber string, currentOwner string, newOwner string) (string, error) {

	fmt.Println("Transfering book with pedigree registration number: " + pedigreeRegistrationNumber + " To: " + newOwner)
	bookAsBytes, err := stub.GetState(pedigreeRegistrationNumber)
	if err != nil {
		return "Failed to get book:", err
	} else if bookAsBytes == nil {
		return "Book does not exist", err
	}

	bookToTransfer := book{}
	err = json.Unmarshal(bookAsBytes, &bookToTransfer) //unmarshal it aka JSON.parse()
	if err != nil {
		return "", err
	}

	if currentOwner != bookToTransfer.Owner {
		return "This book is currently owned by another entity.", err
	}

	bookToTransfer.Owner = newOwner //change the owner

	bookJSONBytes, _ := json.Marshal(bookToTransfer)
	err = stub.PutState(pedigreeRegistrationNumber, bookJSONBytes) //rewrite the book
	if err != nil {
		return "", err
	}

	return "", nil
}

func (t *BookTraceChaincode) getHistoryForRecord(stub shim.ChaincodeStubInterface, args []string) peer.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	recordKey := args[0]

	fmt.Printf("- start getHistoryForRecord: %s\n", recordKey)

	resultsIterator, err := stub.GetHistoryForKey(recordKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing historic values for the key/value pair
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")

		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString("\"")
		buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		buffer.WriteString("\"")

		buffer.WriteString(", \"IsDelete\":")
		buffer.WriteString("\"")
		buffer.WriteString(strconv.FormatBool(response.IsDelete))
		buffer.WriteString("\"")

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getHistoryForRecord returning:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}